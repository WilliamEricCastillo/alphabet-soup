const fs = require('fs');

const readFileModule = (filePath, encoding) => {
    try {
        return fs.readFileSync(filePath, encoding);
    } catch (error) {
        console.error(error.message);
        throw error;
    }
};

const processLinesModule = (fileData, startIndex = 0) => {
    return fileData
        .trim()
        .split('\n')
        .slice(startIndex);
};

module.exports = { readFileModule, processLinesModule };
