const {readFileModule, processLinesModule} = require("./readFileModule");

// create game board matrix
const parseGameBoard = (fileData) => {
    const gameBoardLines = processLinesModule(fileData);

    const [rows, cols] = gameBoardLines[0]
        .trim()
        .split('x')
        .map(Number);

    return gameBoardLines
        .slice(1, rows + 1)
        .map(line => line
        .split(' '));
};

// create key matrix
const parseAnswerKey = (gbGrid, fileData) => {
    return processLinesModule(fileData, gbGrid.length + 1)
        .map(keyLine => keyLine
        .replace(/\s/g, '')
        .split(''));
};

// search game board matrix with key matrix
const keyGenerator = (board, words) => {
    const rows = board.length;
    const cols = board[0].length;

    const searchDirections = [
        [0, 1], // horizontal
        [1, 0], // vertical
        [1, 1], // diagonal
        [-1, 0], // reverse vertical
        [0, -1], // reverse horizontal
        [-1, -1], // reverse diagonal
        [-1, 1], // reverse diagonal
        [1, -1], // reverse diagonal
    ];

    words.forEach((wordRow) =>
    {
        wordRow.forEach((wordCol) =>
        {
            for (let row = 0; row < rows; row++)
            {
                for (let col = 0; col < cols; col++)
                {
                    if (board[row][col] === wordCol)
                    {
                        for (const [rowDir, colDir] of searchDirections)
                        {
                            let currentRow = row;
                            let currentCol = col;
                            let found = true;

                            for (const char of wordRow)
                            {
                                if (board[currentRow][currentCol] !== char)
                                {
                                    found = false;
                                    break;
                                }

                                currentRow += rowDir;
                                currentCol += colDir;
                            }

                            if (found)
                            {
                                console.log(`${wordRow.join('')} ${row}:${col} ${currentRow - rowDir}:${currentCol - colDir}`);
                                break;
                            }
                        }
                    }
                }
            }
        });
    });
};

const filePath = "sampleinput.txt";
const encoding = 'ascii';

try{
    const fileData = readFileModule(filePath, encoding);
    // console.log(fileData)

    const gameBoard = parseGameBoard(fileData)
    // console.log(gameBoard);

    const key = parseAnswerKey(gameBoard, fileData);
    // console.log(key);

    keyGenerator(gameBoard, key);

}catch (error){
    process.exit(1);
}
